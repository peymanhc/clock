import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Modal, Button } from "react-bootstrap";

import "./App.scss";
import Hr from "./components/hr/hr";
import Min from "./components/min/Min";
import Sec from "./components/sec/Sec";

function App() {
  const [date, setdate] = useState(new Date());
  const [show, setShow] = useState(false);

  const tick = () => {
    setdate(new Date());
  };

  const handleClose = () => {
    setShow(false)
    setInterval(() => tick(), 0);
  };
  const handleShow = () => {
    setShow(true);
    setInterval(() => 0);
  }
  
  const hoursDegrees = date.getHours() * 30 + date.getMinutes() / 2;
  const minutesDegrees = date.getMinutes() * 6 + date.getSeconds() / 10;
  const secondsDegrees = date.getSeconds() * 6;

  const Hur = () => {
    return Math.floor(hoursDegrees / 30);
  };
  const Mn = () => {
    return Math.floor(minutesDegrees / 6);
  };
  const Sc = () => {
    return secondsDegrees / 6;
  };
  return (
    <div>
      <div className="clock-container styling">
        <div id="clock" className="clock-content">
          <Hr />
          <Min />
          <Sec />
        </div>
      </div>
      <button className="getTimebtn" onClick={handleShow}>
        Show Time
      </button>
      
      <Modal show={show} onHide={handleClose}>
        <Modal.Header>
          <Modal.Title>This is the time</Modal.Title>
        </Modal.Header>
        <Modal.Body><Hur />:<Mn />:<Sc /></Modal.Body>
      </Modal>
    </div>
  );
}

export default App;


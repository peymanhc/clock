import React, { useState, useEffect } from "react";

function Sec() {
  const [date, setdate] = useState(new Date());

  const tick = () => {
    setdate(new Date());
  };
  const timerID = setInterval(() => tick(), 1000);

  useEffect(() => {
    return timerID;
  }, []);

  const secondsDegrees = date.getSeconds() * 6;

  const divStyleSeconds = {
    transform: "rotateZ(" + secondsDegrees + "deg)",
  };

  return (
    <div
      id="seconds-indicator"
      className={
        "indicator seconds-indicator " +
        (date.getSeconds() === 0 ? "" : "transition-effect")
      }
      style={divStyleSeconds}
    ></div>
  );
}

export default Sec;

import React, { useState, useEffect } from "react";

function Min() {
  const [date, setdate] = useState(new Date());

  const tick = () => {
    setdate(new Date());
  };
  const timerID = setInterval(() => tick(), 1000);

  useEffect(() => {
    return timerID;
  }, []);

  const minutesDegrees = date.getMinutes() * 6 + date.getSeconds() / 10;

  const divStyleMinutes = {
    transform: "rotateZ(" + minutesDegrees + "deg)",
  };

  return (
    <div
    id="minutes-indicator"
    className={
      "indicator minutes-indicator " +
      (date.getMinutes() === 0 ? "" : "transition-effect")
    }
    style={divStyleMinutes}></div>
  );
}

export default Min;

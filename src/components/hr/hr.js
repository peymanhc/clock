import React, { useState, useEffect } from "react";

function Hr() {
  const [date, setdate] = useState(new Date());

  const tick = () => {
    setdate(new Date());
  };
  const timerID = setInterval(() => tick(), 1000);

  useEffect(() => {
    return timerID;
  }, []);

  const hoursDegrees = date.getHours() * 30 + date.getMinutes() / 2;

  const divStyleHours = {
    transform: "rotateZ(" + hoursDegrees + "deg)",
  };

  return (
    <div
      id="hours-indicator"
      className={
        "indicator hours-indicator " +
        (date.getHours() === 0 ? "" : "transition-effect")
      }
      style={divStyleHours}
    ></div>
  );
}

export default Hr;
